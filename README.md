# Formation Intégration Continue avec GitLab et Docker

```yaml
cpp:
  image: ubuntu:16.04
  before_script:
    - apt-get update && apt-get install -y build-essential cmake
  script:
    - cd cpp-demo
    - mkdir build && cd build
    - cmake ..
    - make
    - make test
  artifacts:
    paths:
      - cpp-demo/build/src/DemoApp

angular:
  image: alexsuch/angular-cli:1.2.0
  script:
    - cd angular-demo
    - npm install
    - ng build --prod
  artifacts:
    paths:
      - angular-demo/dist
```
